import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


PACKAGES = (
    'boost-devel',
    'libcurl-devel',
    'glib2-devel',
    'hdf5-devel',
    'libffi-devel',
    'libjpeg-turbo-devel',
    'libtiff-devel',
    'libusb-devel',
    'libusbx-devel',
    'libxml2-devel',
    'libxml2-python',
    'perl-ExtUtils-ParseXS',
    'perl-Pod-Checker',
    'python-devel',
    'readline-devel',
    're2c',
    'systemd-devel',
)


def test_default(Package):
    for item in PACKAGES:
        assert Package(item).is_installed
