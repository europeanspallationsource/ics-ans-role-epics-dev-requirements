ics-ans-role-epics-dev-requirements
===================================

Ansible role to install the base packages required for EPICS development.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

None.
The variable `repository_installroot` comes from the role ics-ans-role-repository.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-epics-dev-requirements
```

License
-------

BSD 2-clause
